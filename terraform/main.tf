resource "kibana_role" "kibana_observability" {
  name = "kibana_observability"
  kibana {
    spaces = ["*"]

    features {
      name        = "actions"
      permissions = ["all"]
    }
    features {
      name        = "dashboard"
      permissions = ["all"]
    }
    features {
      name        = "discover"
      permissions = ["all"]
    }
    features {
      name        = "stackAlerts"
      permissions = ["all"]
    }
    features {
      name        = "visualize"
      permissions = ["all"]
    }
  }
}

resource "elasticsearch_xpack_role_mapping" "test" {
  role_mapping_name = "observability"
  enabled = true
  roles = [
    kibana_role.kibana_observability.name,
    "viewer",
  ]
  rules   = <<-EOF
  {
    "all": [
      {
        "field": {
          "realm.name": "REALM_NAME"
        }
      },
      {
        "field": {
          "groups": "REALM_GROUP"
        }
      }
    ]
  }
  EOF
}

resource "elasticstack_elasticsearch_security_user" "kibana_observability" {
  username = "kibana_observability"
  password = "kibana_observability"
  roles    = ["viewer", kibana_role.kibana_observability.name]
}

