terraform {
  required_providers {
    elasticstack = {
      source  = "elastic/elasticstack"
      version = "0.4.0"
    }
    elasticsearch = {
      source  = "phillbaker/elasticsearch"
      version = "2.0.5"
    }
    kibana = {
      source  = "disaster37/kibana"
      version = "8.0.1"
    }
  }
}

provider "elasticstack" {
  elasticsearch {
    username  = "elastic"
    password  = "elastic"
    endpoints = ["https://localhost:9200"]
    insecure  = true
  }
}

provider "elasticsearch" {
  url        = "https://localhost:9200"
  kibana_url = "http://localhost:5601"
  username   = "elastic"
  password   = "elastic"
  insecure   = true
  healthcheck = false
}

provider "kibana" {
  url      = "http://localhost:5601"
  username = "elastic"
  password = "elastic"
}
