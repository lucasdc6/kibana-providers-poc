locals {
  spaces = jsondecode(file("${path.module}/spaces.json"))
}

resource "kibana_user_space" "test" {
  for_each          = { for space in local.spaces : space.name => space }
  uid               = each.value.id
  name              = each.key
  description       = lookup(each.value, "description", "")
  initials          = lookup(each.value, "initials", "")
  color             = lookup(each.value, "color", "#ffffff")
  disabled_features = lookup(each.value, "disabledFeatures", [])
}

