# Kibana providers POC


## TL;TR

```console
docker-compose up -d
cd terraform
terraform init
terraform apply
# Or without terraform installed locally
./bin/terraform init
./bin/terraform apply
# Or in windows
./bin/terraform.ps1 init
./bin/terraform.ps1 apply
```

## docker-compose

Install and configure an elasticsearch with kibana. The `setup` container create
the ssl certificates needed by the alert and connection configuration (check
https://www.elastic.co/guide/en/kibana/8.4/alert-action-settings-kb.html#alert-action-settings-kb),
and setup the kibana password.

